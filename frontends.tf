### Variables
variable front_instance_number {
  default = "2"
}

variable front_ami {
  default = "ami-0d77397e" # Ubuntu 16.04
}

variable front_instance_type {
  default = "t2.micro"
}

variable public_key {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC3WAphQjjx/1OhBnvxgeVfpNV7h5fqIuJlS1IJ6BYqkJ7wjSqXS5UwQTDe2CzcBWYFYBjv9F2fsDN3aJ8v0+aKBfhuBLOqjb8qlxpnpqYz4UauDh7DDnTE4UZ4iXu4dYCVvIgvK5Nb4Q5/6KHencx3OqNcy+MJX5vaf+RrT5J34GHYOResCmvnHT0vLspAcYkVhi4au2fIff+N1jcXQB2gGhMyicuCDhL/aHPpNKs6xknqXI5ewfJEkfdUdJssXIYVcbqneYrOAgEVwwqQPXWEIX2LdcrYx9VPp+P2PmJlJBCd+GkZiD4hwdlCgpKoAZOOLoXX974b0GTWm7w1cLiRqU7lsw2zHFpzN4UthlA/38pJUOKklFuJp5dWWXFmI+t+/+3TrerCBUoZlDZ/6KaSCswbAo+jilM8kcIn2+7AZr62isOn/f4nE+Wv7ms+CVCZ1A3YAWn9GuIrxzrXK1HCqJQZWd0zWV/gmRNc1GNZcscllMCIgtDZt8Q4s0bvzN8= mazigh.abdedou@outlook.fr"
}

variable front_elb_port {
  default = "80"
}

variable front_elb_protocol {
  default = "http"
}

### Resources
resource "aws_key_pair" "front" {
  key_name   = "${var.project_name}-front"
  public_key = "${var.public_key}"
}

resource "aws_instance" "front" {
  # TO DO
  # see https://www.terraform.io/docs/providers/aws/r/instance.html
  count                  = "${var.front_instance_number}"
  ami                    = "${var.front_ami}"
  instance_type          = "${var.front_instance_type}"
  subnet_id              = "${aws_subnet.public.*.id[count.index]}"
  vpc_security_group_ids = ["${aws_security_group.front.id}"]
  key_name               = "${aws_key_pair.front.key_name}"

  tags = {
    Name = "${var.project_name}-front-${count.index}"
  }
}

resource "aws_elb" "front" {
  # TO DO
  # see https://www.terraform.io/docs/providers/aws/r/elb.html
  name            = "${var.project_name}-front-elb"
  subnets         = ["${aws_subnet.public.*.id}"]
  security_groups = ["${aws_security_group.elb.id}"]
  instances       = ["${aws_instance.front.*.id}"]

  listener {
    instance_port     = "${var.front_elb_port}"
    instance_protocol = "${var.front_elb_protocol}"
    lb_port           = "${var.front_elb_port}"
    lb_protocol       = "${var.front_elb_protocol}"
  }

  tags = {
    Name = "${var.project_name}-front-elb"
  }
}

### Outputs
output "elb_endpoint" {
  # TO DO
  # see https://www.terraform.io/intro/getting-started/outputs.html
  value = "${aws_elb.front.dns_name}"
}

output "instance_ip" {
  # TO DO
  value = ["${aws_instance.front.*.public_ip}"]
}
